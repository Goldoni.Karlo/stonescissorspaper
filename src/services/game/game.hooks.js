function hookdata() {
  return (hook) => {console.log(hook.data);
                    hook.data.remote=getRemoteChoise();
                    hook.data.mark=getReferyVerdict(hook.data.people, hook.data.remote)};
}

function getRemoteChoise()
{
 // Реализуем выбор компьютерной стороны 
 // выбор случайного числа взят отсюда https://learn.javascript.ru/task/random-int-min-max
let positions=['Камень','Ножницы','Бумага'];
let remoteChoise= 0 - 0.5 + Math.random() * 3;
return positions [Math.round(remoteChoise)];
}

function getReferyVerdict(peopleChoise, remoteChoise)
{
// Реализация алгоритма присуждения победы или поражения для игрока человека
 let mark='Ничья';
 if ((peopleChoise=='Камень')&&(remoteChoise=='Ножницы')) {mark='Победа'}
 if ((peopleChoise=='Камень')&&(remoteChoise=='Бумага')) {mark='Поражение'}
 if ((peopleChoise=='Ножницы')&&(remoteChoise=='Бумага')) {mark='Победа'}
 if ((peopleChoise=='Ножницы')&&(remoteChoise=='Камень')) {mark='Поражение'}
 if ((peopleChoise=='Бумага')&&(remoteChoise=='Камень')) {mark='Победа'}
 if ((peopleChoise=='Бумага')&&(remoteChoise=='Ножницы')) {mark='Поражение'}
return mark;
}

function hooksort(){ 
  return (hook) => { hook.params.query.$sort={_id: 1}}
}

module.exports = {
  before: {
    all: [],
    find: [],
    get: [ hooksort()],
    create: [hookdata()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
