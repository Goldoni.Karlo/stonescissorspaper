console.log ('Testing');

import test from 'ava';
import supertest from 'supertest';
const app = require('./_app');
const request = supertest(app);

module.exports = {};

// Variables for testing 
var newTurnId='';


test.serial('get turns', async t => {
  const response= await request
    .get('/game')
    .set('content-type', 'application/json')
    .expect(200);
    t.is(response.statusCode, 200)
    const turns=response.body.data;
    for (var i=0; i<turns.length; i++)
        {
            console.log (turns[i].people, turns[i].remote, turns[i].mark);
            const response2= await request
            .get('/game/'+turns[i]._id)
            .set('content-type', 'application/json')
            .expect(200);
            t.is(response2.statusCode, 200)
        }
});

test.serial('create turn', async t  => {
  const response = await request
  .post('/game')
  .set('content-type', 'application/json')
  .send({people: 'Hren', remote: 'redki', mark: 'ne slasche', time:'7.40'})
  .expect(201);
  t.is(response.statusCode, 201)
  }
);

test.serial('patch turn', async t => {
  const response= await request
  .get('/game')
  .set('content-type', 'application/json')
  .expect(200);
  t.is(response.statusCode, 200)
  const turns=response.body.data;
  const turn_patch = turns.find((element) => element.people === 'Hren');
  // Get newTurnId
  newTurnId=turn_patch._id;
  const response2 = await request
  .patch('/game/'+turn_patch._id)
  .set('content-type', 'application/json')
  .send({ people: 'HREN',remote: 'REDKI'})
  .expect(200);
  t.is(response2.statusCode, 200)
});

test.serial('put turn', async t => {
  const response = await request
  .put('/game/'+newTurnId)
  .set('content-type', 'application/json')
  .send({ people: 'Gorky hren', remote: 'Beloy redki', mark: 'nikak ne slashche', time: '7654321' })
  .expect(200);
  t.is(response.statusCode, 200)
});

test.serial('delete turn', async t => {
  const response = await request
    .delete('/game/'+newTurnId)
    .set('content-type', 'application/json')
    .expect(200); 
  t.is(response.statusCode, 200)    
 });
 
 test.serial('delete all turns', async t => {
  const response = await request
    .delete('/game/')
    .set('content-type', 'application/json')
    .expect(200); 
  t.is(response.statusCode, 200)    
 });