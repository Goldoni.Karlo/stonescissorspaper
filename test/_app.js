const http = require('http');
const app = require('../src/app');
const server = http.createServer((request, response) => {
    response.end('Hello Node.js Server!');
});
app.setup(server);

module.exports = app;
